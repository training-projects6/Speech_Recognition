//
//  Speech_RecognitionApp.swift
//  Speech_Recognition
//
//  Created by Artem Soloviev on 22.06.2023.
//

import SwiftUI

@main
struct Speech_RecognitionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
